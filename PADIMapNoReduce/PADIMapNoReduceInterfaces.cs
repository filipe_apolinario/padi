﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PADIMapNoReduce
{
    public interface IMapper
    {
        IList<KeyValuePair<string, string>> Map(string fileLine);
    }

    public interface IMapperTransfer
    {
        bool SendMapper(byte[] code, string className);
        byte[] getMapperHash();
        String getMapperName();
    }

    public interface IJobTracker : IMapperTransfer
    {
        void SubmitJob(int numSplits, String clientUrl);
        void ReportWorkProgress(string workerUrl, int currWorkPhase, int split, int linesDone, int totalLines);
        void ReportWorkDone(string workerUrl, int split, int totalLines);

        // Puppet Master Helper Methods
        void FreezeC();
        void UnfreezeC();
    }

    public interface IWorkerService : IJobTracker
    {
        void Meet(String url);
        ICollection<string> GetOtherWorkersUrls();
        void AssignWork(int splitId, String clientUrl, String jobTrackerUrl, String bestFriendUrl, String backupUrl);
        void StopWorking();
        void SendInputBackup(String[] input);
        String[] GetInputBackup();
        void SendMapBackup(IList<KeyValuePair<String, String>> mapBackup, int linesMapped);
        IList<KeyValuePair<String, String>> GetMapBackup();
        int GetLinesMappedBackup();

        // Puppet Master Helper Methods
        void SlowW(int delayInSeconds);
        void PrintStatus();
        void FreezeW();
        void UnfreezeW();

    }

    public interface IClientService
    {
        String[] GetInputData(int splitId);
        void SubmitResult(int splitId, IList<KeyValuePair<string, string>> output);
    }

    public interface IPuppetMasterService
    {
        void createNewWorker(int id, String serviceUrl, String entryURL);
    }
}
