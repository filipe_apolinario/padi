%
%  $Description: Author guidelines and sample document in LaTeX 2.09$ 
%
%  $Author: ienne $
%  $Date: 1995/09/15 15:20:59 $
%  $Revision: 1.4 $
%

\documentclass[times, 10pt,twocolumn]{article} 
\usepackage{latex8}
\usepackage{times}
\usepackage[utf8x]{inputenc}
\usepackage{listings}
\usepackage{graphicx}

%\documentstyle[times,art10,twocolumn,latex8]{article}

%------------------------------------------------------------------------- 
% take the % away on next line to produce the final camera-ready version 
\pagestyle{empty}

%------------------------------------------------------------------------- 
\begin{document}

\title{PADIMapNoReduce}

\author{Filipe Apolinário\\
Instituto Superior Técnico\\
f.apolinario30@gmail.com\\
\and
Jorge Goulart\\
jorgesimaoag@gmail.com\\
\and
Pedro Silva\\
pedro.mgd.silva@hotmail.com\\
}

\maketitle
\thispagestyle{empty}

\begin{abstract}
In this document, we will describe how we implemented the PADIMapNoReduce system, a system that implements the Map function of a regular MapReduce system. In this system, a Client can submit a job to the system, composed of a number of Workers. These will map the input as specified by the Client, and will then return the result to the Client. To ensure efficiency and prevent loss of data, we also implemented a way to detect failures (and respond accordingly) and a replication system that allows a Worker to continue off where another Worker had its failure.
\end{abstract}



%------------------------------------------------------------------------- 
\Section{Introduction}

With the evolution of the internet, it became fairly easy to connect more and more computers, allowing to group the computational power of each of this individual units. One way to achieve this is using MapReduce. MapReduce is a programming model for processing and generating large data sets with a distributed algorithm on a cluster. The Map procedure performs filtering and sorting while the Reduce procedure performs a summary operation. This paper presents an implementation of the Map procedure, composed of multiple machines performing the work.

%------------------------------------------------------------------------- 
\Section{PADIMapNoReduce Architecture}

\subsection{System Components}

The system is composed of multiple servers that act as JobTrackers and Workers simultaneously, and a client that invokes these servers. In order to be able to control these multiple servers and to simulate clients making requests, there is one (or more) PuppetMaster that coordinate a group of servers.
\begin{figure}[h]
	\label{fig:overview}
	\includegraphics[width=0.5\textwidth]{Threads}
	\caption{Overview of the system}
\end{figure}

\subsubsection{Client}

The client is the one who submits the jobs to a worker node (this one will be named Job Tracker, and have a special function during the job).

For simplicity, there is only one client at a time, and thus only one job at a time.

The client is the only one who has access to the input file used during the job, and, therefore, is the one responsible for dividing the input file into multiple splits. Because of this, Worker nodes can request the client for a split at any time.

To allow the use of very big files in submitted jobs, the client never loads the entire file into memory. Instead, every time a worker asks for a split, the client creates a FileStream. Based on the size of the file in bytes and the number of splits, we determine an approximate split size, split start and split end.
\begin{lstlisting}[language=C]
splitSize = (inputLength + numSplits - 1) 
			/ numSplits;
splitStart = splitSize * splitId;
splitEnd = splitStart + splitSize - 1;
\end{lstlisting}
Then, based on these approximations, we find the real split start by searching for the first occurrence of the character \texttt{/n} after \texttt{splitStart}. To find the real split end, we employ the same strategy, only instead starting at \texttt{splitEnd}. Finally, to find the real split size, we just subtract these two values.

At any time, a worker can submit the result of its mapping phase for a split, being then saved to disk. Since writing to disk is a very time-intensive procedure, every time the client receives a new result, it launches a new thread that will write that information to disk. This allows for the worker to continue working on other splits while the client is writing its previous splits to disk. If the client receives the same split more than once, it will ignore every result submission after the first one.
In order to ensure that the client only finishes once the entire job has terminated and the splits have been written to disk, we use a semaphore: The client application is blocked by the semaphore until is has been signaled once for every split. In other words, it will block at the semaphore \texttt{numSplits} times. This signal is sent every time that a thread finishes writing to disk.



%in order to allow very big files (bigger than 1GB) to be used for a submitted job, the client never loads the entire file into memory. Instead, every time a worker asks for a split, the client creates a FileStream. Based on the size of the file in bytes, and the number of splits, we determine an approximate split size, split start and split end (insert calculations to determine these approximations). Then, based on these approximations, we find the real split start, end and size, by (...)
%At any time, a worker can then submit the result of it's mapping phase for a split. The result is then saved in disk.
%Since writing to disk is a very time-intensive procedure, every time the client receives a new result, it launches a new thread that will then write it to disk. This allows for the worker to continue working on other splits while the client is writing it's split to disk.
%if the client receives the same split more than once, it will ignore every result submission after the first one.
%in order to ensure the client is only released once the entire job is finished, and the splits have been written to disk, we use a semaphore. every time a thread finishes writing to disk, it signals the semaphore. the client application will be blocked by the semaphore until is has been signaled once for every split (in other words, it will block at the semaphore numSplit times)

\subsubsection{JobTracker}

The job tracker is the node that is contacted by the client, and has a different responsibility to all other worker nodes in the system for this job. Once the client submits a job, the Job Tracker creates three different collections:
\begin{itemize}
  \item one to hold the Available Workers (i.e. those that currently have no split assigned to them)
  \item one to hold the Busy Workers (i.e. those that are currently assigned to a split, and are active)
  \item one to hold the Frozen Workers (i.e. those that have a split assigned to them, but show no signs of being active)
\end{itemize}
\begin{figure}[h]
	\label{fig:worker_state}
	\includegraphics[width=0.5\textwidth]{Worker_State}
	\caption{The Worker nodes in the system can be in one of three states. The JobTracker closely monitors the Workers, and makes decisions based on their current state, which can result in a change of state}
\end{figure}


Two threads are also created: The Assigner and the Janitor.

The Assigner is the thread responsible to assign different splits to different workers. To do so, it first starts by fetching an Available Worker. If there is no such worker, it will block itself until a worker finishes and becomes Available. Then, it will search for an unassigned split and assign it to the worker. It continues doing this until all splits have been finished.
To assign a split to a worker, it must first check whether the worker has the correct map implementation or not. To do so, all workers have stored a hash of the implementation, and the name of the one to use. The Assigner requests the hash and name of the mapper implementation currently held by the worker and compares them to the ones used for the current job. If they are not equal, then it sends the mapper implementation. Afterwards, it informs the worker of which split to work on, where is the client located (it's URL) and who is the JobTracker for this job. Finally, it adds the worker to the Busy Workers collection.

The Janitor is the thread that monitors the activity of the busy Workers, and determines if there are failures in the Workers. To do so, we implemented a heartbeat system, where busy Workers will periodically inform the JobTracker of their progress in the split they are assigned to. The Janitor will, periodically, wake up and check when was the last time every busy Worker reported its progress. If a considerable amount of time has passed with no communication from the Worker, the Janitor will consider it to be frozen, remove it from the Busy Workers, and add it to the Frozen Workers. Then, it will signal that the split the Frozen Worker was working on is now unassigned, to allow the Assigner to pick it up and assign to a new worker. Once the Frozen Worker recovers and starts sending heartbeats, the JobTracker removes it from the Frozen Worker collection and checks whether the split has been assigned to a different Worker while the first one was Frozen. If no other Worker was assigned, then the previously Frozen Worker is moved to the Busy Worker collection once again, and work continues as if nothing happened. If a new Worker did indeed take over the split, then the new Worker's progress is compared to the previously Frozen Worker's progress. The Worker with the greater number of lines processed is allowed to continue the work, while the other one is asked to abort work on the split, and is moved to the Available Worker collection.

%The job tracker is the node that is contacted by the client, and has a different responsibility to all other worker nodes in the system for this job.
%once the client submits a job, the Job Tracker creates three different collections: one to hold the Available Workers (i.e. those that currently have no split assigned to them), one to hold the Busy Workers (i.e. those that are currently assigned to a split, and are active), and the Frozen Workers (i.e. those that have a split assigned to them, but show no signs of being active)
%two threads are also created: the Assigner and the Janitor.
%The assigner is the thread responsible to assign different splits to different workers.
%to do so, it first starts by fetching an Available Worker. If there is no such worker, it will block itself until a worker finishes and becomes Available. Then, it will search for an unassigned split and assign it to the worker. It continues doing this until all splits have been finished.
%to assign a split to a worker, it must first check whether the worker has the correct map implementation. To do so, all workers have stored and hash of the implementation, and the name of the one to use. The Assigner requests the hash and name of the mapper implementation currently held by the worker and compares them to the one used for the current job. If they are not equal, then it sends the mapper implementation.
%afterwards, it informs the worker of which split to work on, where is the client located (it's UrL) and who is the JobTracker for this job.
%finally, it adds the worker to the Busy Workers collection
%The Janitor is the thread that monitors the activity of the busy workers, and determines whether a worker is frozen and stopped working.
%to do so, we implemented a heartbeat system, where busy workers will periodically inform the jobtracker of their progress in the split they are assigned to.
%The janitor will, periodically, wake up and check when was the last time every busy worker reported its progress. If a considerable amount of time has passed with no communication from the worker, the Janitor will consider it to be frozen, remove it from the Busy Workers, and add it to the Frozen Workers. Then, it will signal that the split the Frozen Worker was working on is now unassigned, to allow the Assigner to pick it up and assign to a new worker
%once the Frozen Worker goes up again, and starts sending heartbeats, the JobTracker then removed it from the frozen worker collection and checks whether the split has since been assigned to a different worker.
%If no other worker was assigned, then the previously Frozen Worker is moved to the Busy Worker collection once again, and work continues as if nothing happened.
%If a new worker did indeed take over the split, then the new worker's progress is compared to the previously frozen worker's progress. The worker with the greater number of lines processed is allowed to continue the work, while the other one is asked to abort work on the split, and is moved to the Available Worker collection

\subsubsection{Worker}

When the JobTracker assigns a Split to a Worker, two threads are created: the Reporter and the Mapper.

The Reporter will periodically report the progress on the split to the JobTracker, thus serving as a heartbeat. It will continuously do so until it is stopped by the Mapper.

The Mapper is the thread that will work on the split. It first starts by asking the client for the input. Then, for each line of the input, it will use the mapper implementation given previously to map each line of the output. Finally, it will return the output of the mapping phase to the client, stop the Reporter, and inform the JobTracker that the split has finished, and the worker is once again Available
%When the JobTracker assigns a Split to a Worker, two threads are created: the Mapper ad the Reporter
%the Mapper is the thread that will work on the split. It first starts by asking the client for the input. Then, for each line of the input, it will use the mapper implementation given previously to map each line of the output. Finally, it will return the output of the mapping phase to the client, stop the Reporter, and inform the JobTracker that the split hasbeen finished, and the worker is once again Available
%the Reporter will periodically report the progress on the split to the JobTracker, thus serving as a heartbeat. it will continuously do so until it is stopped by the Mapper

\subsubsection{PuppetMaster}

In order to be able to test the application in a controlled environment, it was necessary to develop the Puppet Master. Although the Puppet Master is not a real component of the system, it has the ability to control what happens in it:
\begin{itemize}
  \item Deploy Workers/JobTrackers into the system
  \item Deploy a Client asking for a certain job to be done
  \item Simulate failures in the Workers
  \item Simulate failures in the Job trackers
  \item Ask all the servers for their status
\end{itemize}



%------------------------------------------------------------------------- 
\SubSection{Fault Tolerance}

\subsubsection{Workers Fault Tolerance}

Since mapping can be very resource intensive, it is valuable to ensure that data is not lost in the case of a Worker failure.
To implement that, each Worker has a Best Friend. A Worker's Best Friend is the one responsible to hold a backup of the input file and a partial result of the mapping phase. Every Worker only has one Best Friend, and each Worker is the best Friend of only one other Worker.

The Job Tracker is the one responsible to dictate each Worker's Best Friend. Once a job is submitted, the Job tacker assigns a best friend for all workers, such that the previous requirements are met. A way to implement this is described in Figure \ref{fig:choosing_best_friends}.
\begin{figure}[h]
	\includegraphics[width=0.5\textwidth]{Choosing_Best_Friends}
	\caption{A possible way for a JobTracker to choose each Worker's Best Friend would be to layout the Workers in a circle. Each Worker's Best Friend would be the one next to it in the circle. The actual order of the Workers is irrelevant. The JobTracker doesn't have a Best Friend, since it doesn't need to backup input or mapping data.}
	\label{fig:choosing_best_friends}
\end{figure}

Once a worker is assigned with a split, it is informed of who will be it's Best Friend. It will then proceed with its normal operation. However, once the input has been fetched from the client, the Worker will try to send it to its Best Friend. It will do so asynchronously, by launching a separate thread.
Likewise, once the Worker has reached a certain threshold during its mapping phase, it will try to send the achieved result to its Best Friend, as it did with the input.

However, it is possible that a Worker's Best Friend is down. If after some time the Worker sees that his Best Friend is not responding, it gives up on making a back up.

In case of a Worker failure, the JobTracker can check whether the split has a backup. If it does, when assigning it to a new worker, the JobTracker will inform the worker where the backup it's located. The Worker will then attempt to obtain the input and the partial result from the backup, instead of going to the client and starting the mapping phase from scratch. However, the backup can be frozen. Just like before, the worker will attempt to get the back up, but if after some time, there is no answer, it will give up and start as if no backup was made. On the other hand, the backup can be frozen. Just like before, the Worker will attempt to get the backup, but if after some time there is no answer, it will give up and start as if no backup was made.
\begin{figure}[h]
	\includegraphics[width=0.5\textwidth]{Best_Friend_and_Back_Up}
	\caption{In case a Worker is tasked to continue the work of a Frozen Worker, he will proceed with the following steps: (1) Ask the Back Up Worker (i.e. the Frozen Worker's Best Friend) for the input file (2) Back Up will return the input file (3) Ask the Back Up for the partial result that was kept (4) Back Up will return all the pairs that were saved (5) Make a back up on the input file at its Best Friend (6) Continue with the mapping, starting where the back up was last made (7) Once the threshold is reached, back up the result.}
	\label{fig:best_friend_and_back_up}
\end{figure}

%Since mapping can be very resource intensive, it is valuable to ensure that data is not lost in the case of worker failure.
%to implement that, we made it s that each worker has a Best Friend.
%A worker's Best Friend is the one responsble to hold a back of the input file and a partial result of the mapping phase.
%Every worker only has one Best Friend, and each worker is the Best Friend of only one other worker.
%The job tracker is the one responsible to dictate each worker's best friend. Once a job is submitted, it assigns a best friend for all workers, such that the requirements are met
%Once a worker is assigned with a split, it is informed of who will be it's best friend.
%Once input has been fetched from the client, a worker will try to send it to its best friend. It will do so asynchronously, by launching a separate thread.
%Likewise, once the worker has reached a certain threshold during its mapping phase, it will try to send it to its best friend, as it did with the input
%However, it is possible that a worker's best friend is down. If after some time the worker sees his best friend is not responding, it gives up on making a back up.
%In case of a Worker failure, the JobTracker can check whether the split has a backup. If it does, when assigning it to a new worker, the JobTracker will informed the worker where the backup it's located.
%The worker will then attempt to retrieve the input and the partial result from the backup, instead of going to the client and starting the mapping phase from scratch
%However, the backup can be frozen. Just like before, the worker will attempt to get the back up, but if after some time, there is no answer, it will give up and start as if no backup was made.

\subsubsection{JobTrackers Fault Tolerance}

This feature was not implemented in our system. However, this could be made using the same principle that was used for the Workers Fault Tolerance. By assigning a Best Friend to the JobTracker and storing in the Best Friend replicated data, the system would be able to deal with failures in the JobTracker.

%------------------------------------------------------------------------- 
\Section{Conclusions}

The solution presented in this paper can be used in several and distinct situations, since it was designed to be scalable and robust. It can be subject to optimization, as required for some specific problems.

%------------------------------------------------------------------------- 


\end{document}