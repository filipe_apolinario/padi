﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PADIMapNoReduce
{
    public partial class PuppetMasterForm : Form
    {
        private PuppetMasterControls puppetMasterControls;
        
        public PuppetMasterForm()
        {
            puppetMasterControls = new PuppetMasterControls(this);
            InitializeComponent();
        }

        private void commandSubmitButton_Click(object sender, EventArgs e)
        {
            puppetMasterControls.RunCommand(commandTextBox.Text);
            commandTextBox.Text = "";
        }

        public void WriteLog(String log)
        {
            logTextBox.Text += log + "\r\n";
        }

        private void fileBrowserButton_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            openFileDialog1.InitialDirectory = "c:\\";
            openFileDialog1.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
            openFileDialog1.FilterIndex = 2;
            openFileDialog1.RestoreDirectory = true;

            DialogResult result = openFileDialog1.ShowDialog(); // Show the dialog.
            if (result == DialogResult.OK) // Test result.
            {
                string file = openFileDialog1.FileName;
                scriptFileTextBox.Text = file;
            }
        }

        private void scriptFileLoadButton_Click(object sender, EventArgs e)
        {
            String file = scriptFileTextBox.Text;
            puppetMasterControls.LoadFile(file);
        }

        private void scriptRunButton_Click(object sender, EventArgs e)
        {
            puppetMasterControls.RunScript();
        }

        private void scriptNextStepButton_Click(object sender, EventArgs e)
        {
            puppetMasterControls.StepScript();
        }
      
    }
}
