﻿namespace PADIMapNoReduce
{
    partial class PuppetMasterForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.scriptGroupBox = new System.Windows.Forms.GroupBox();
            this.scriptRunButton = new System.Windows.Forms.Button();
            this.scriptNextStepButton = new System.Windows.Forms.Button();
            this.scriptFileLoadButton = new System.Windows.Forms.Button();
            this.scriptFileTextBox = new System.Windows.Forms.TextBox();
            this.scriptFileLabel = new System.Windows.Forms.Label();
            this.commandGroupBox = new System.Windows.Forms.GroupBox();
            this.commandSubmitButton = new System.Windows.Forms.Button();
            this.commandTextBox = new System.Windows.Forms.TextBox();
            this.commandLabel = new System.Windows.Forms.Label();
            this.logGroupBox = new System.Windows.Forms.GroupBox();
            this.logTextBox = new System.Windows.Forms.TextBox();
            this.fileBrowserButton = new System.Windows.Forms.Button();
            this.scriptGroupBox.SuspendLayout();
            this.commandGroupBox.SuspendLayout();
            this.logGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // scriptGroupBox
            // 
            this.scriptGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.scriptGroupBox.Controls.Add(this.fileBrowserButton);
            this.scriptGroupBox.Controls.Add(this.scriptRunButton);
            this.scriptGroupBox.Controls.Add(this.scriptNextStepButton);
            this.scriptGroupBox.Controls.Add(this.scriptFileLoadButton);
            this.scriptGroupBox.Controls.Add(this.scriptFileTextBox);
            this.scriptGroupBox.Controls.Add(this.scriptFileLabel);
            this.scriptGroupBox.Location = new System.Drawing.Point(12, 12);
            this.scriptGroupBox.Name = "scriptGroupBox";
            this.scriptGroupBox.Size = new System.Drawing.Size(589, 118);
            this.scriptGroupBox.TabIndex = 0;
            this.scriptGroupBox.TabStop = false;
            this.scriptGroupBox.Text = "Script";
            // 
            // scriptRunButton
            // 
            this.scriptRunButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.scriptRunButton.Location = new System.Drawing.Point(108, 89);
            this.scriptRunButton.Name = "scriptRunButton";
            this.scriptRunButton.Size = new System.Drawing.Size(93, 23);
            this.scriptRunButton.TabIndex = 4;
            this.scriptRunButton.Text = "Run Script";
            this.scriptRunButton.UseVisualStyleBackColor = true;
            this.scriptRunButton.Click += new System.EventHandler(this.scriptRunButton_Click);
            // 
            // scriptNextStepButton
            // 
            this.scriptNextStepButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.scriptNextStepButton.AutoSize = true;
            this.scriptNextStepButton.Location = new System.Drawing.Point(9, 89);
            this.scriptNextStepButton.Name = "scriptNextStepButton";
            this.scriptNextStepButton.Size = new System.Drawing.Size(93, 23);
            this.scriptNextStepButton.TabIndex = 3;
            this.scriptNextStepButton.Text = "Next Step";
            this.scriptNextStepButton.UseVisualStyleBackColor = true;
            this.scriptNextStepButton.Click += new System.EventHandler(this.scriptNextStepButton_Click);
            // 
            // scriptFileLoadButton
            // 
            this.scriptFileLoadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.scriptFileLoadButton.Location = new System.Drawing.Point(508, 58);
            this.scriptFileLoadButton.Name = "scriptFileLoadButton";
            this.scriptFileLoadButton.Size = new System.Drawing.Size(75, 23);
            this.scriptFileLoadButton.TabIndex = 2;
            this.scriptFileLoadButton.Text = "Load";
            this.scriptFileLoadButton.UseVisualStyleBackColor = true;
            this.scriptFileLoadButton.Click += new System.EventHandler(this.scriptFileLoadButton_Click);
            // 
            // scriptFileTextBox
            // 
            this.scriptFileTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.scriptFileTextBox.Location = new System.Drawing.Point(6, 32);
            this.scriptFileTextBox.Name = "scriptFileTextBox";
            this.scriptFileTextBox.Size = new System.Drawing.Size(496, 20);
            this.scriptFileTextBox.TabIndex = 1;
            // 
            // scriptFileLabel
            // 
            this.scriptFileLabel.AutoSize = true;
            this.scriptFileLabel.Location = new System.Drawing.Point(6, 16);
            this.scriptFileLabel.Name = "scriptFileLabel";
            this.scriptFileLabel.Size = new System.Drawing.Size(23, 13);
            this.scriptFileLabel.TabIndex = 0;
            this.scriptFileLabel.Text = "File";
            // 
            // commandGroupBox
            // 
            this.commandGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.commandGroupBox.Controls.Add(this.commandSubmitButton);
            this.commandGroupBox.Controls.Add(this.commandTextBox);
            this.commandGroupBox.Controls.Add(this.commandLabel);
            this.commandGroupBox.Location = new System.Drawing.Point(12, 136);
            this.commandGroupBox.Name = "commandGroupBox";
            this.commandGroupBox.Size = new System.Drawing.Size(589, 96);
            this.commandGroupBox.TabIndex = 1;
            this.commandGroupBox.TabStop = false;
            this.commandGroupBox.Text = "Command";
            // 
            // commandSubmitButton
            // 
            this.commandSubmitButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.commandSubmitButton.Location = new System.Drawing.Point(508, 58);
            this.commandSubmitButton.Name = "commandSubmitButton";
            this.commandSubmitButton.Size = new System.Drawing.Size(75, 23);
            this.commandSubmitButton.TabIndex = 2;
            this.commandSubmitButton.Text = "Submit";
            this.commandSubmitButton.UseVisualStyleBackColor = true;
            this.commandSubmitButton.Click += new System.EventHandler(this.commandSubmitButton_Click);
            // 
            // commandTextBox
            // 
            this.commandTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.commandTextBox.Location = new System.Drawing.Point(6, 32);
            this.commandTextBox.Name = "commandTextBox";
            this.commandTextBox.Size = new System.Drawing.Size(577, 20);
            this.commandTextBox.TabIndex = 1;
            // 
            // commandLabel
            // 
            this.commandLabel.AutoSize = true;
            this.commandLabel.Location = new System.Drawing.Point(6, 16);
            this.commandLabel.Name = "commandLabel";
            this.commandLabel.Size = new System.Drawing.Size(54, 13);
            this.commandLabel.TabIndex = 0;
            this.commandLabel.Text = "Command";
            // 
            // logGroupBox
            // 
            this.logGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.logGroupBox.Controls.Add(this.logTextBox);
            this.logGroupBox.Location = new System.Drawing.Point(12, 238);
            this.logGroupBox.Name = "logGroupBox";
            this.logGroupBox.Size = new System.Drawing.Size(589, 294);
            this.logGroupBox.TabIndex = 2;
            this.logGroupBox.TabStop = false;
            this.logGroupBox.Text = "Log";
            // 
            // logTextBox
            // 
            this.logTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.logTextBox.Location = new System.Drawing.Point(6, 20);
            this.logTextBox.Multiline = true;
            this.logTextBox.Name = "logTextBox";
            this.logTextBox.ReadOnly = true;
            this.logTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.logTextBox.Size = new System.Drawing.Size(577, 268);
            this.logTextBox.TabIndex = 0;
            // 
            // fileBrowserButton
            // 
            this.fileBrowserButton.Location = new System.Drawing.Point(508, 30);
            this.fileBrowserButton.Name = "fileBrowserButton";
            this.fileBrowserButton.Size = new System.Drawing.Size(75, 23);
            this.fileBrowserButton.TabIndex = 5;
            this.fileBrowserButton.Text = "Browse...";
            this.fileBrowserButton.UseVisualStyleBackColor = true;
            this.fileBrowserButton.Click += new System.EventHandler(this.fileBrowserButton_Click);
            // 
            // PuppetMasterForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(613, 544);
            this.Controls.Add(this.logGroupBox);
            this.Controls.Add(this.commandGroupBox);
            this.Controls.Add(this.scriptGroupBox);
            this.Name = "PuppetMasterForm";
            this.Text = "PuppetMaster";
            this.scriptGroupBox.ResumeLayout(false);
            this.scriptGroupBox.PerformLayout();
            this.commandGroupBox.ResumeLayout(false);
            this.commandGroupBox.PerformLayout();
            this.logGroupBox.ResumeLayout(false);
            this.logGroupBox.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox scriptGroupBox;
        private System.Windows.Forms.Button scriptFileLoadButton;
        private System.Windows.Forms.TextBox scriptFileTextBox;
        private System.Windows.Forms.Label scriptFileLabel;
        private System.Windows.Forms.GroupBox commandGroupBox;
        private System.Windows.Forms.Button scriptRunButton;
        private System.Windows.Forms.Button scriptNextStepButton;
        private System.Windows.Forms.Button commandSubmitButton;
        private System.Windows.Forms.TextBox commandTextBox;
        private System.Windows.Forms.Label commandLabel;
        private System.Windows.Forms.GroupBox logGroupBox;
        private System.Windows.Forms.TextBox logTextBox;
        private System.Windows.Forms.Button fileBrowserButton;
    }
}

