﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Reflection;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Tcp;

using PADIMapNoReduce;
using System.Collections.ObjectModel;
using System.Security.Cryptography;
using System.Net;
using System.Net.Sockets;

namespace Worker
{
    class Worker
    {
        static void Main(string[] args)
        {
            String serviceURL = args[0];
            String entryUrl = (args.Length > 1) ? args[1] : null;
            WorkerService ws = new WorkerService();
            ws.Init(serviceURL, entryUrl);

            System.Console.WriteLine("<enter> para sair...");
            System.Console.ReadLine();
        }
    }

    class WorkerService : MarshalByRefObject, IWorkerService
    {
        // Data about myself
        TcpChannel channel;
        String myUrl;

        // Mapper Implementation
        private byte[] mapperCode;
        private string mapperName;
        private byte[] mapperHash;
        private Type mapperType;
        private Object mapperObj;

        // Connections to other nodes in the system
        private IDictionary<String, IWorkerService> otherWorkers;

        // Current work state
        private String clientUrl;
        private String jobTrackerUrl;
        String[] inputLines;
        private int totalLines;
        private int currentSplit;
        private int currentLine;
        IList<KeyValuePair<String, String>> mappingResult;
        private bool stopWorking;
        private enum WorkPhases { NotWorking, GettingInput, Mapping, ReturningOutput, JobTracking };
        private int currentWorkPhase = (int)WorkPhases.NotWorking;

        // Backup info
        private String bestFriendUrl;
        private String backupUrl;
        private String[] inputBackup = null;
        private IList<KeyValuePair<String, String>> mapBackup = null;
        private int linesMappedBackup = 0;
        private bool backupRetrievalSuccess = false;

        // JobTracking Info
        private int numSplits;
        private SplitInfo[] splitBuffer;
        private AvailableWorkersQueue availableWorkers;
        private IDictionary<String, WorkerInfo> busyWorkers;
        private IDictionary<String, WorkerInfo> frozenWorkers;
        private EventWaitHandle splitFree = new EventWaitHandle(false, EventResetMode.AutoReset);
        private IDictionary<String, String> bestFriends;

        // Puppet Master helpers
        private int slowW = 0;
        private bool freezeW = false;
        private EventWaitHandle unfreezeWEvent = new EventWaitHandle(true, EventResetMode.ManualReset);
        private bool freezeC = false;
        private EventWaitHandle unfreezeCEvent = new EventWaitHandle(true, EventResetMode.ManualReset);

        /***************************************************************\
         *                        INIT METHODS                         *
        \***************************************************************/
        public void Init(String serviceUrl, String entryUrl)
        {
            String[] serviceUrlArgs = serviceUrl.Split(new char[] { ':', '/' }, StringSplitOptions.RemoveEmptyEntries);

            String protocol = serviceUrlArgs[0];

            String ip = serviceUrlArgs[1];
            if (ip.Equals("localhost"))
            {
                IPHostEntry host;
                host = Dns.GetHostEntry(Dns.GetHostName());
                foreach (IPAddress ipAddress in host.AddressList)
                {
                    if (ipAddress.AddressFamily == AddressFamily.InterNetwork)
                    {
                        ip = ipAddress.ToString();
                    }
                }
            }

            int port = Int32.Parse(serviceUrlArgs[2]);

            String objUri = serviceUrlArgs[3];

            myUrl = protocol + "://" + ip + ":" + port + "/" + objUri;

            System.Collections.IDictionary dict = new System.Collections.Hashtable();
            dict["port"] = port;
            dict["timeout"] = 10 * 1000;
            channel = new TcpChannel(dict, null, new BinaryServerFormatterSinkProvider());
            ChannelServices.RegisterChannel(channel, true);
            RemotingServices.Marshal(this, objUri, typeof(IWorkerService));

            otherWorkers = new Dictionary<String, IWorkerService>();
            if (entryUrl != null)
            {
                String[] entryUrlArgs = entryUrl.Split(new char[] { ':', '/' }, StringSplitOptions.RemoveEmptyEntries);
                String entryProtocol = entryUrlArgs[0];
                String entryIp = entryUrlArgs[1];
                if (entryIp.Equals("localhost"))
                {
                    entryIp = ip;
                }
                int entryPort = Int32.Parse(entryUrlArgs[2]);
                String entryObjUri = entryUrlArgs[3];
                entryUrl = entryProtocol + "://" + entryIp + ":" + entryPort + "/" + entryObjUri;

                this.Meet(entryUrl);
                IWorkerService otherWorker;
                otherWorkers.TryGetValue(entryUrl, out otherWorker);
                otherWorker.Meet(myUrl);

                ICollection<String> othersUrls = otherWorker.GetOtherWorkersUrls();
                foreach (String url in othersUrls)
                {
                    this.Meet(url);
                    if (otherWorkers.TryGetValue(url, out otherWorker))
                    {
                        otherWorker.Meet(myUrl);
                    }
                }
            }
        }

        public void Meet(string url)
        {
            if (!(otherWorkers.ContainsKey(url) || url.Equals(myUrl)))
            {
                IWorkerService otherWorker = (IWorkerService)Activator.GetObject(
                    typeof(IWorkerService),
                    url);
                otherWorkers.Add(url, otherWorker);
                System.Console.WriteLine("Met " + url);
            }
        }


        public ICollection<string> GetOtherWorkersUrls()
        {
            return otherWorkers.Keys;
        }

        /***************************************************************\
         *                      MAPPER METHODS                         *
        \***************************************************************/
        public bool SendMapper(byte[] code, string className)
        {
            mapperCode = code;
            mapperName = className;
            mapperHash = MD5.Create().ComputeHash(code);

            Assembly assembly = Assembly.Load(code);
            // Walk through each type in the assembly looking for our class
            IEnumerable<Type> assTypes = GetTypes(assembly);
            System.Console.WriteLine("Assembly with ");
            foreach (Type type in assTypes)
            {
                System.Console.WriteLine(type);
                if (type.IsClass == true)
                {
                    if (type.FullName.EndsWith("." + className))
                    {
                        // create an instance of the object
                        mapperObj = Activator.CreateInstance(type);
                        mapperType = type;
                        System.Console.WriteLine("Received and saved " + className);
                        System.Console.WriteLine("Mapper Hash is ");
                        foreach (byte b in mapperHash)
                        {
                            System.Console.Write(b + " ");
                        }
                        System.Console.WriteLine();
                        return true;
                    }
                }
            }
            throw (new System.Exception("could not invoke method"));
        }

        private IEnumerable<Type> GetTypes(Assembly assembly)
        {
            if (assembly == null) throw new ArgumentNullException("assembly");
            try
            {
                return assembly.GetTypes();
            }
            catch (ReflectionTypeLoadException e)
            {
                return e.Types.Where(t => t != null);
            }
        }

        public byte[] getMapperHash()
        {
            return mapperHash;
        }

        public String getMapperName()
        {
            return mapperName;
        }

        private IList<KeyValuePair<string, string>> InvokeMapMethod(String fileLine)
        {
            object[] args = new object[] { fileLine };
            object resultObject = mapperType.InvokeMember("Map",
              BindingFlags.Default | BindingFlags.InvokeMethod,
                   null,
                   mapperObj,
                   args);

            IList<KeyValuePair<string, string>> result = (IList<KeyValuePair<string, string>>)resultObject;

            /*
            Console.WriteLine("Map call result was: ");
            foreach (KeyValuePair<string, string> p in result)
            {
                Console.WriteLine("key: " + p.Key + ", value: " + p.Value);
            }
            */

            return result;
        }

        /***************************************************************\
         *                     JOB TACKER METHODS                      *
        \***************************************************************/
        public void SubmitJob(int numSplits, String clientUrl)
        {
            freezeJobTracker();
            slow();

            currentWorkPhase = (int)WorkPhases.JobTracking;
            this.clientUrl = clientUrl;
            this.jobTrackerUrl = myUrl;
            this.numSplits = numSplits;
            System.Console.WriteLine(clientUrl + " submited job of " + numSplits + " split(s).");


            // add available workers to queue
            availableWorkers = new AvailableWorkersQueue(otherWorkers.Keys);
            busyWorkers = new Dictionary<String, WorkerInfo>();
            frozenWorkers = new Dictionary<String, WorkerInfo>();
            bestFriends = new Dictionary<String, String>();

            freezeJobTracker();
            slow();

            //assign best friends
            String[] workersArray = otherWorkers.Keys.ToArray<String>();
            int numWorkers = workersArray.Length;
            for (int i = 0; i < numWorkers; i++)
            {
                bestFriends.Add(workersArray[i], workersArray[((i + 1) % numWorkers)]);
            }

            freezeJobTracker();
            slow();

            splitBuffer = new SplitInfo[numSplits];
            for (int i = 0; i < numSplits; i++)
            {
                splitBuffer[i] = new SplitInfo(i);
            }


            freezeJobTracker();
            slow();

            Thread assigner = new Thread(WorkAssignerTask);
            assigner.Start();

            Thread janitor = new Thread(TakingOutTheTrash);
            janitor.Start();


            freezeJobTracker();
            slow();

            assigner.Join();
            janitor.Join();

            currentWorkPhase = (int)WorkPhases.NotWorking;

            freezeJobTracker();
            slow();
        }

        private void WorkAssignerTask()
        {
            freezeJobTracker();
            slow();
         
            while (true)
            {
                freezeJobTracker();
                slow();
                String workerUrl = availableWorkers.GetAvailableWorker();
                int result;
                do
                {
                    freezeJobTracker();
                    slow();
                    result = TryFindAndAssignSplit(workerUrl);
                    // buffer is empty - job done!
                    if (result == 0)
                    {
                        return;
                    }

                    // Split assigned to a worker!
                    if (result == 1)
                    {
                        break;
                    }

                    // All splits were working - wait for a bit and try again
                    splitFree.WaitOne();
                } while (result == 2);

            }
        }

        private bool IsSplitBufferEmpty()
        {
            foreach (SplitInfo s in splitBuffer)
            {
                if (s != null) { return false; }
            }
            return true;
        }

        private int TryFindAndAssignSplit(String workerUrl)
        {
            lock (splitBuffer)
            {
                if (!IsSplitBufferEmpty())
                {
                    foreach (SplitInfo s in splitBuffer)
                    {
                        if (s != null && !s.CurrentlyWorking)
                        {
                            //check if someone was previously working, if so, get backupWorker
                            String backupUrl = null;
                            if (s.PreviousWorkerUrl != null)
                            {
                                bestFriends.TryGetValue(s.PreviousWorkerUrl, out backupUrl);
                            }

                            IWorkerService worker;
                            otherWorkers.TryGetValue(workerUrl, out worker);

                            int currSplit = s.ID;
                            s.Worker = worker;
                            s.WorkerUrl = workerUrl;

                            WorkerInfo busyWorkerInfo = new WorkerInfo(workerUrl, worker, currSplit, DateTime.Now);

                            busyWorkers.Add(workerUrl, busyWorkerInfo);

                            Thread assign = new Thread(AssignWorkToWorker);
                            KeyValuePair<int, String[]> arguments = new KeyValuePair<int, String[]>(currSplit, new String[] { workerUrl, backupUrl });
                            assign.Start((object)arguments);

                            return 1;
                        }
                    }
                    return 2;
                }
                return 0;
            }
        }

        private void AssignWorkToWorker(object argumentObj)
        {
            KeyValuePair<int, String[]> args = (KeyValuePair<int, String[]>)argumentObj;
            int currSplit = args.Key;
            String[] values = args.Value;
            String workerUrl = values[0];
            String backupUrl = values[1];

            IWorkerService worker;
            otherWorkers.TryGetValue(workerUrl, out worker);


            // Check if this worker has current mapper implementation
            // Start by assuming they are equal
            bool mapperIsEqual = true;
            byte[] workerMapperHash = worker.getMapperHash();
            if (workerMapperHash != null)
            {
                // check all bytes of hash
                for (int i = 0; i < workerMapperHash.Length; i++)
                {
                    // if any are different, mapper is not equal
                    if (workerMapperHash[i] != mapperHash[i])
                    {
                        mapperIsEqual = false;
                        break;
                    }
                }

                // if all bytes were equal, mapper may still be different - check if it has the same mapper name
                if (mapperIsEqual)
                {
                    mapperIsEqual = worker.getMapperName().Equals(mapperName);
                }
            }
            else
            {
                // if there is no mapper in worker node, it needs to be sent
                mapperIsEqual = false;
            }

            // only send mapper if worker has different mapper implementation
            if (!mapperIsEqual)
            {
                worker.SendMapper(mapperCode, mapperName);
            }

            String bestFriendUrl;
            bestFriends.TryGetValue(workerUrl, out bestFriendUrl);

            worker.AssignWork(currSplit, clientUrl, myUrl, bestFriendUrl, backupUrl);
        }

        public void ReportWorkProgress(String workerUrl, int currWorkPhase, int split, int linesDone, int totalLines)
        {
            freezeJobTracker();
            slow();

            System.Console.WriteLine(workerUrl + " Split#" + split + " (" + linesDone + "/" + totalLines + " " + WorkPhaseToString(currWorkPhase) + ")");
            if (splitBuffer[split] != null)
            {
                lock (splitBuffer[split])
                {
                    splitBuffer[split].LinesDone = linesDone;
                    splitBuffer[split].TotalLines = totalLines;
                }
            }

            WorkerInfo worker;
            if (busyWorkers.TryGetValue(workerUrl, out worker))
            {
                worker.LastReport = DateTime.Now;
                worker.LinesDone = linesDone;
                worker.WorkPhase = currWorkPhase;
            }
            else
            {
                // did a dead thread come back?
                if (frozenWorkers.TryGetValue(workerUrl, out worker))
                {
                    RiseOfTheDead(worker);
                }

                // if not, then this is probably a useless report
            }

            freezeJobTracker();
            slow();
        }

        public void ReportWorkDone(String workerUrl, int split, int totalLines)
        {
            freezeJobTracker();
            slow();
         
            System.Console.WriteLine(workerUrl + " Split#" + split + " (" + totalLines + "/" + totalLines + " DONE)");

            lock (splitBuffer)
            {
                splitBuffer[split] = null;
            }
            busyWorkers.Remove(workerUrl);
            availableWorkers.AddAvailableWorker(workerUrl);

            //send event
            splitFree.Set();

            freezeJobTracker();
            slow();
        }


        private void TakingOutTheTrash()
        {
            while (!IsSplitBufferEmpty())
            {
                DateTime ThirtySecsAgo = DateTime.Now.AddSeconds(-30);
                foreach (WorkerInfo i in (new Dictionary<String, WorkerInfo>(busyWorkers)).Values)
                {
                    if (DateTime.Compare(i.LastReport, ThirtySecsAgo) < 0)
                    {
                        System.Console.WriteLine("Worker " + i.WorkerURL + ": you're dead to me.");
                        //delete busy worker
                        frozenWorkers.Add(i.WorkerURL, i);
                        busyWorkers.Remove(i.WorkerURL);
                        splitBuffer[i.WorkingOnSplit].TotalLines = -1;
                        splitBuffer[i.WorkingOnSplit].LinesDone = 0;
                        splitBuffer[i.WorkingOnSplit].Worker = null;
                        splitBuffer[i.WorkingOnSplit].WorkerUrl = null;

                        //send event
                        splitFree.Set();
                    }
                }
                System.Threading.Thread.Sleep(5 * 1000);
            }
        }

        private void RiseOfTheDead(WorkerInfo zombie)
        {
            //get split the zombie was doing
            int split = zombie.WorkingOnSplit;
            frozenWorkers.Remove(zombie.WorkerURL);
            zombie.LastReport = DateTime.Now;

            //if split is already finished, make worker available
            if (splitBuffer[split] == null)
            {
                zombie.Worker.StopWorking();
                availableWorkers.AddAvailableWorker(zombie.WorkerURL);
                return;
            }

            // check if there is not another worker doing it
            if (splitBuffer[split].Worker == null)
            {
                busyWorkers.Add(zombie.WorkerURL, zombie);
                return;
            }
            //if there is another worker doing it
            //if zombie has done more work
            if (zombie.LinesDone > splitBuffer[split].LinesDone)
            {
                //order current worker to stop
                splitBuffer[split].Worker.StopWorking();
                //and make it available
                busyWorkers.Remove(splitBuffer[split].WorkerUrl);
                splitBuffer[split].Worker = zombie.Worker;
                splitBuffer[split].WorkerUrl = zombie.WorkerURL;
                splitBuffer[split].LinesDone = zombie.LinesDone;
                availableWorkers.AddAvailableWorker(splitBuffer[split].WorkerUrl);
                busyWorkers.Add(zombie.WorkerURL, zombie);
                return;
            }
            else
            {
                zombie.Worker.StopWorking();
                availableWorkers.AddAvailableWorker(zombie.WorkerURL);
                return;
            }

        }

        /***************************************************************\
         *                       WORKER METHODS                        *
        \***************************************************************/
        public void AssignWork(int splitId, String clientUrl, String jobTrackerUrl, String bestFriendUrl, String backupUrl)
        {
            freezeWorker();
            slow();

            // Init progress status
            this.clientUrl = clientUrl;
            this.jobTrackerUrl = jobTrackerUrl;
            this.bestFriendUrl = bestFriendUrl;
            this.backupUrl = backupUrl;
            currentSplit = splitId;
            currentLine = 0;
            totalLines = -1;
            stopWorking = false;

            Thread workingThread = new Thread(WorkingThread);

            freezeWorker();
            slow();

            workingThread.Start();
        }

        private void WorkingThread()
        {
            System.Console.WriteLine("Assigned Split#" + currentSplit);

            // Create WorkReporterThread
            Thread workReporter = new Thread(WorkReport);
            workReporter.Start();

            // find Client
            currentWorkPhase = (int)WorkPhases.GettingInput;
            IClientService client = (IClientService)Activator.GetObject(
                typeof(IClientService),
                clientUrl);



            //if there is a backup, get input from it
            mappingResult = null;
            inputLines = null;
            currentLine = 0;
            backupRetrievalSuccess = false;
            if (backupUrl != null)
            {
                System.Console.WriteLine("Retrieving BackUp from " + backupUrl);
                // if i have my own backup
                if (backupUrl.Equals(myUrl))
                {
                    inputLines = this.inputBackup;
                    mappingResult = this.mapBackup;
                    currentLine = this.linesMappedBackup;
                }
                else
                {
                    // someone else has the backup - lets try to get it back
                    Thread backupRetreiver = new Thread(getAllBackups);
                    backupRetreiver.Start();
                    backupRetreiver.Join();

                    if (!backupRetrievalSuccess)
                    {
                        // backup worker is probably down - we have to start from scratch!
                        mappingResult = null;
                        inputLines = null;
                        currentLine = 0;
                    }
                }
            }


            // else get Input from Client
            if (inputLines == null)
            {
                inputLines = client.GetInputData(currentSplit);
            }
            if (mappingResult == null)
            {
                mappingResult = new List<KeyValuePair<String, String>>();
                currentLine = 0;
            }

            totalLines = inputLines.Length;

            // try to do a backup...
            Thread inputBackupMaker = new Thread(doInputBackup);
            inputBackupMaker.Start();

            // run map function
            currentWorkPhase = (int)WorkPhases.Mapping;
            bool backedup = false;
            Thread mapperBackUpMaker = new Thread(doMapperBackup);

            for (int i = currentLine; i < inputLines.Length; i++)
            {

                freezeWorker();
                slow();

                String line = inputLines[i];

                IList<KeyValuePair<String, String>> r = InvokeMapMethod(line);
                foreach (KeyValuePair<String, String> kvp in r)
                {
                    //FIXME falta verificar se estamos a adicionar keys que ja existem
                    mappingResult.Add(kvp);
                }
                currentLine++;

                // did JobTracker ask me to stop?
                if (stopWorking)
                {
                    System.Console.WriteLine("//!\\\\ Aborting work...");
                    currentWorkPhase = (int)WorkPhases.NotWorking;
                    workReporter.Abort();
                    workReporter.Join();
                    return;
                }

                // backup my data once I have more than 1000 lines and 50% calculated
                if (!backedup && currentLine > 1000 && currentLine > (totalLines / 2))
                {
                    backedup = true;
                    IList<KeyValuePair<String, String>> mappingResultCopy = new List<KeyValuePair<String, String>>(mappingResult);
                    KeyValuePair<int, IList<KeyValuePair<String, String>>> backupArgs = new KeyValuePair<int, IList<KeyValuePair<String, String>>>(currentLine, mappingResultCopy);
                    mapperBackUpMaker.Start(backupArgs);
                }
            }

            freezeWorker();
            slow();

            // return output to Client
            currentWorkPhase = (int)WorkPhases.ReturningOutput;
            client.SubmitResult(currentSplit, mappingResult);

            freezeWorker();
            slow();

            inputBackupMaker.Join();
            if (backedup)
            {
                mapperBackUpMaker.Join();
            }

            // Job is done, time to stop the reporting
            workReporter.Abort();
            workReporter.Join();
            currentWorkPhase = (int)WorkPhases.NotWorking;
            IWorkerService jobTracker;
            otherWorkers.TryGetValue(jobTrackerUrl, out jobTracker);
            jobTracker.ReportWorkDone(myUrl, currentSplit, totalLines);

            System.Console.WriteLine("Split#" + currentSplit + " is DONE");

            freezeWorker();
            slow();
        }

        private void WorkReport()
        {
            IWorkerService jobTracker;
            otherWorkers.TryGetValue(jobTrackerUrl, out jobTracker);

            while (true)
            {
                freezeWorker();
                slow();
                jobTracker.ReportWorkProgress(myUrl, currentWorkPhase, currentSplit, currentLine, totalLines);
                System.Threading.Thread.Sleep(5 * 1000);
            }
        }

        public void StopWorking()
        {
            freezeWorker();
            slow();

            stopWorking = true;

            freezeWorker();
            slow();
        }

        public void SendInputBackup(String[] lines)
        {
            freezeWorker();
            slow();
            this.inputBackup = lines;
            freezeWorker();
            slow();
        }

        public String[] GetInputBackup()
        {
            freezeWorker();
            slow();
            return this.inputBackup;
        }

        public void SendMapBackup(IList<KeyValuePair<String, String>> mapBackup, int linesMapped)
        {
            freezeWorker();
            slow();
            this.mapBackup = mapBackup;
            this.linesMappedBackup = linesMapped;
            freezeWorker();
            slow();
        }

        public IList<KeyValuePair<String, String>> GetMapBackup()
        {
            freezeWorker();
            slow();
            return this.mapBackup;
        }

        public int GetLinesMappedBackup()
        {
            freezeWorker();
            slow();
            return this.linesMappedBackup;
        }

        private void getAllBackups()
        {
            IWorkerService backupWorker;
            otherWorkers.TryGetValue(backupUrl, out backupWorker);
            try
            {
                this.inputLines = backupWorker.GetInputBackup();
                this.currentLine = backupWorker.GetLinesMappedBackup();
                this.mappingResult = backupWorker.GetMapBackup();
            }
            catch (Exception)
            {
                System.Console.WriteLine(backupUrl + " is unavailable - not able to retrieve backup");
                return;
            }
            backupRetrievalSuccess = true;
        }

        private void doInputBackup()
        {
            IWorkerService bestfriend;
            otherWorkers.TryGetValue(bestFriendUrl, out bestfriend);

            try
            {
                bestfriend.SendInputBackup(inputLines); // Input Lines will not be altered, thus it's safe to read
            }
            catch (Exception)
            {
                System.Console.WriteLine(bestFriendUrl + " is unavailable - not able to make a backup of input");
            }
        }

        private void doMapperBackup(object mapResultObj)
        {
            KeyValuePair<int, IList<KeyValuePair<String, String>>> mapResultPair = (KeyValuePair<int, IList<KeyValuePair<String, String>>>)mapResultObj;
            int linesMapped = mapResultPair.Key;
            IList<KeyValuePair<String, String>> mapResult = mapResultPair.Value;
            IWorkerService bestfriend;
            otherWorkers.TryGetValue(bestFriendUrl, out bestfriend);

            try
            {
                bestfriend.SendMapBackup(mapResult, currentLine);
            }
            catch (Exception)
            {
                System.Console.WriteLine(bestFriendUrl + " is unavailable - not able to make a backup of map result");
            }
        }

        /***************************************************************\
         *                 PUPPET MASTER HELPER METHODS                *
        \***************************************************************/
        public void PrintStatus()
        {
            System.Console.WriteLine("----------------------------------BEGIN STATUS----------------------------------");

            System.Console.WriteLine("My URL: " + myUrl);
            System.Console.Write("Current Phase: " + WorkPhaseToString(currentWorkPhase));
            if (freezeW) { System.Console.Write(" FROZEN "); }
            if (slowW > 0) { System.Console.Write(" SLOW "); }
            System.Console.WriteLine();

            if (currentWorkPhase == (int)WorkPhases.NotWorking)
            {
                System.Console.WriteLine("-----------------------------------END STATUS-----------------------------------");
                return;
            }

            System.Console.WriteLine("Client: " + clientUrl);
            System.Console.WriteLine("JobTracker: " + jobTrackerUrl);

            if (currentWorkPhase == (int)WorkPhases.JobTracking)
            {
                System.Console.WriteLine("Num Splits: " + numSplits);

                System.Console.WriteLine();
                System.Console.WriteLine("BUSY WORKERS:");
                foreach (WorkerInfo wi in busyWorkers.Values)
                {
                    System.Console.WriteLine(wi.WorkerURL + " (Split#" + wi.WorkingOnSplit + " Line#" + wi.LinesDone + " " + WorkPhaseToString(wi.WorkPhase) + ")");
                    String bestFriendUrl = null;
                    bestFriends.TryGetValue(wi.WorkerURL, out bestFriendUrl);
                    System.Console.WriteLine("\tBest Friend @ " + bestFriendUrl);
                }

                System.Console.WriteLine();
                System.Console.WriteLine("FROZEN WORKERS:");
                foreach (WorkerInfo wi in frozenWorkers.Values)
                {
                    System.Console.WriteLine(wi.WorkerURL + " (Split#" + wi.WorkingOnSplit + " Line#" + wi.LinesDone + " " + WorkPhaseToString(wi.WorkPhase) + ")");
                    String bestFriendUrl = null;
                    bestFriends.TryGetValue(wi.WorkerURL, out bestFriendUrl);
                    System.Console.WriteLine("\tBest Friend @ " + bestFriendUrl);
                }
                System.Console.WriteLine("----------------------------------END STATUS-----------------------------------");
                return;
            }

            System.Console.WriteLine("Split#" + currentSplit);
            System.Console.WriteLine(currentLine + "/" + totalLines);
            System.Console.WriteLine("Best Friend @ " + this.bestFriendUrl);
            if (backupUrl != null)
            {
                System.Console.WriteLine("This split's BackUp @ " + backupUrl);
            }
            System.Console.WriteLine("----------------------------------END STATUS-----------------------------------");
        }

        private string WorkPhaseToString(int workPhase)
        {
            switch (workPhase)
            {
                case (int)WorkPhases.NotWorking:
                    return "Not Working";
                case (int)WorkPhases.GettingInput:
                    return "Getting Input";
                case (int)WorkPhases.Mapping:
                    return "Mapping";
                case (int)WorkPhases.ReturningOutput:
                    return "Returning Output";
                case (int)WorkPhases.JobTracking:
                    return "Job Tracking";
                default:
                    return "Error";
            }
        }

        public void SlowW(int delayInSeconds)
        {
            System.Console.WriteLine("SLOW " + delayInSeconds);
            slowW = delayInSeconds;
        }


        public void FreezeW()
        {
            System.Console.WriteLine("FREEZEW");
            unfreezeWEvent.Reset();
            freezeW = true;
        }

        public void UnfreezeW()
        {
            System.Console.WriteLine("UNFREEZEW");
            freezeW = false;
            unfreezeWEvent.Set();
        }

        public void FreezeC()
        {
            System.Console.WriteLine("FREEZEC");
            unfreezeCEvent.Reset();
            freezeC = true;
        }

        public void UnfreezeC()
        {
            System.Console.WriteLine("UNFREEZEC");
            freezeC = false;
            unfreezeCEvent.Set();
        }

        private void freezeWorker()
        {
            while (freezeW)
            {
                unfreezeWEvent.WaitOne();
            }
        }

        private void freezeJobTracker()
        {
            while (freezeC)
            {
                unfreezeCEvent.WaitOne();
            }
        }

        private void slow()
        {
            if (slowW > 0)
            {
                System.Threading.Thread.Sleep(slowW * 1000);
                slowW = 0;
            }
        }
    }

    public class AvailableWorkersQueue
    {
        private readonly Queue<String> availableWorkers;
        private readonly Semaphore signal;

        public AvailableWorkersQueue(ICollection<String> workers)
        {
            availableWorkers = new Queue<String>(workers);
            this.signal = new Semaphore(workers.Count, int.MaxValue);
        }

        public void AddAvailableWorker(String worker)
        {
            lock (this.availableWorkers)
            {
                this.availableWorkers.Enqueue(worker);
            }
            this.signal.Release();
        }

        public String GetAvailableWorker()
        {
            this.signal.WaitOne();
            String worker;
            lock (this.availableWorkers)
            {
                worker = this.availableWorkers.Dequeue();
            }
            return worker;
        }
    }

    public class SplitInfo
    {
        private int id;
        private IWorkerService worker = null;
        private String workerUrl = null;
        private int linesDone = 0;
        private int totalLines = -1;
        private String previousWorkerUrl = null;

        public SplitInfo(int splitId)
        {
            this.id = splitId;
        }

        public int ID
        {
            get { return id; }
        }

        public IWorkerService Worker
        {
            get { return worker; }
            set { worker = value; }
        }

        public String WorkerUrl
        {
            get { return workerUrl; }
            set { previousWorkerUrl = workerUrl; workerUrl = value; }
        }

        public String PreviousWorkerUrl
        {
            get { return previousWorkerUrl; }
        }

        public int LinesDone
        {
            get { return linesDone; }
            set { linesDone = value; }
        }

        public int TotalLines
        {
            get { return totalLines; }
            set { totalLines = value; }
        }

        public bool CurrentlyWorking
        {
            get { return worker != null; }
        }
    }

    public class WorkerInfo
    {
        private string workerUrl;
        private IWorkerService worker;
        private int workingOnSplit;
        private DateTime lastReport;
        private int linesDone;
        private int workPhase;

        public WorkerInfo(string workerUrl, IWorkerService worker, int workingOnSplit, DateTime lastReport)
        {
            this.workerUrl = workerUrl;
            this.worker = worker;
            this.workingOnSplit = workingOnSplit;
            this.lastReport = lastReport;
            this.linesDone = 0;
            workPhase = 0;
        }

        public String WorkerURL
        {
            get { return workerUrl; }
        }

        public IWorkerService Worker
        {
            get { return worker; }
        }

        public int WorkingOnSplit
        {
            get { return workingOnSplit; }
            set { workingOnSplit = value; }
        }

        public DateTime LastReport
        {
            get { return lastReport; }
            set { lastReport = value; }
        }

        public int LinesDone
        {
            get { return linesDone; }
            set { linesDone = value; }
        }

        public int WorkPhase
        {
            get { return workPhase; }
            set { workPhase = value; }
        }
    }

}
