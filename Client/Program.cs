﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Remoting.Channels.Tcp;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Net.Sockets;
using System.IO;
using System.Threading;

using PADIMapNoReduce;
using System.Net;

namespace Client
{
    class Program
    {
        static void Main(string[] args)
        {
            // TODO test number of args

            String entryUrl = args[0];
            String inputFile = args[1];
            String outputPath = args[2];
            int numSplits = Int32.Parse(args[3]);
            String mapperName = args[4];
            String mapperImpl = args[5];
            Client client = new Client();
            client.Init(entryUrl);
            client.SubmitJob(inputFile, numSplits,
                                outputPath,
                                mapperName,
                                mapperImpl);

            System.Console.WriteLine("Terminado.");
            System.Console.ReadLine();
        }
    }

    class Client : MarshalByRefObject, IClientService
    {
        // Connection data
        private TcpChannel channel;
        private String clientIp;
        private int clientPort = 10001;
        private IJobTracker jobTracker;

        // Submited job data
        private String inputFile;
        private long inputLength;
        private int numSplits;
        private String outputDir;

        // result writers
        private Thread[] writters;
        private bool[] alreadyWritten;
        private Semaphore writeSemaphore;

        public void Init(String entryUrl)
        {
            channel = new TcpChannel(clientPort);
            ChannelServices.RegisterChannel(channel, true);

            RemotingServices.Marshal(this, "C", typeof(IClientService));

            jobTracker = (IJobTracker)Activator.GetObject(
                typeof(IJobTracker),
                entryUrl);

            IPHostEntry host;
            host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (IPAddress ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    clientIp = ip.ToString();
                }
            }
        }

        public void SubmitJob(String inputFile, int numSplits, String outputDir, String mapperName, String mapperPath)
        {
            // input file
            this.inputFile = inputFile;
            this.outputDir = outputDir;

            FileStream fileStream = new FileStream(inputFile, FileMode.Open, FileAccess.Read);
            inputLength = fileStream.Length;  // get file length
            fileStream.Close();

            // send Mapper implementation
            byte[] code = File.ReadAllBytes(mapperPath);
            jobTracker.SendMapper(code, mapperName);

            // submit job
            this.numSplits = numSplits;

            writters = new Thread[numSplits];
            alreadyWritten = new bool[numSplits];
            writeSemaphore = new Semaphore(0, numSplits);

            for (int i = 0; i < numSplits; i++)
            {
                writters[i] = new Thread(WriteResultToDisk);
                alreadyWritten[i] = false;
            }

            String clientUrl = "tcp://" + clientIp + ":" + clientPort + "/C";
            System.Console.WriteLine(clientUrl);
            jobTracker.SubmitJob(numSplits, clientUrl);

            for (int i = 0; i < numSplits; i++)
            {
                writeSemaphore.WaitOne();
            }

            foreach (Thread w in writters)
            {
                w.Join();
            }
        }

        public String[] GetInputData(int splitId)
        {
            int splitSize = (int)((inputLength + numSplits - 1) / numSplits);
            long splitStart = splitSize * splitId;
            long splitEnd = splitStart + splitSize - 1;

            // correct for the last split
            splitEnd = splitEnd > (inputLength - 1) ? (inputLength - 1) : splitEnd;

            byte[] buffer;
            FileStream fileStream = new FileStream(inputFile, FileMode.Open, FileAccess.Read);
            try
            {
                // search for the beginning of the first line
                if (splitId != 0)
                {
                    // place position at beginning of the split
                    fileStream.Seek(splitStart, SeekOrigin.Begin);

                    char testChar;
                    do
                    {
                        // advance the position in the stream to the correct place
                        testChar = (char)fileStream.ReadByte();
                        splitStart++;
                    } while (testChar != '\n');
                }

                // search for the end of the last line
                if (splitId != (numSplits - 1))
                {
                    // place position at end of the split
                    fileStream.Seek(splitEnd, SeekOrigin.Begin);

                    char testChar;
                    do
                    {
                        // advance the position in the stream to the correct place
                        testChar = (char)fileStream.ReadByte();
                        splitEnd++;
                    } while (testChar != '\n');
                }

                // correct the size of the split
                splitSize = (int)(splitEnd - splitStart + 1);

                // read the split
                buffer = new byte[splitSize]; // create buffer for the split
                fileStream.Seek(splitStart, SeekOrigin.Begin);
                fileStream.Read(buffer, 0, splitSize); // and read the split
            }
            finally
            {
                fileStream.Close();
            }
            //System.Console.WriteLine("Sending from " + splitStart + " to " + splitEnd + " for Split#" + splitId);

            // transform bytes to string
            string test = System.Text.Encoding.UTF8.GetString(buffer, 0, splitSize);
            // System.Console.WriteLine("----------Text read:\n" + test + "\n----------");
            string[] result = test.Split('\n');
            System.Console.WriteLine("Sending " + result.Length + " lines for Split#" + splitId);

            return result;
        }

        public void SubmitResult(int splitId, IList<KeyValuePair<string, string>> result)
        {
            System.Console.WriteLine("Received result of split#" + splitId + " with " + result.Count + " KeyValuePairs");

            lock (alreadyWritten)
            {
                if (alreadyWritten[splitId] == false)
                {
                    KeyValuePair<int, IList<KeyValuePair<String, String>>> threadArgs = new KeyValuePair<int, IList<KeyValuePair<string, string>>>(splitId, result);
                    writters[splitId].Start(threadArgs);
                    alreadyWritten[splitId] = true;
                }
            }
        }

        private void WriteResultToDisk(object argsObj)
        {
            KeyValuePair<int, IList<KeyValuePair<String, String>>> args = (KeyValuePair<int, IList<KeyValuePair<String,String>>>) argsObj;
            int splitId = args.Key;
            IList<KeyValuePair<String, String>> result = args.Value;

            // save result to new file in outputDir
            String[] output = new String[result.Count];
            int i = 0;
            foreach (KeyValuePair<String, String> pair in result)
            {
                output[i++] = "key: " + pair.Key + ", value: " + pair.Value;
                // System.Console.WriteLine(output[i - 1]);
            }
            System.IO.File.WriteAllLines(outputDir + (splitId + 1) + ".out", output);
            writeSemaphore.Release();
        }
    }
}
